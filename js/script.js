document.addEventListener("DOMContentLoaded", function() {
    const menuItems = document.querySelectorAll("nav ul li");

    menuItems.forEach(item => {
        item.addEventListener("click", () => {
            menuItems.forEach(item => item.classList.remove("active"));
            item.classList.add("active");
        });

        const subMenu = item.querySelector("ul");
        if (subMenu) {
            item.addEventListener("mouseenter", () => {
                subMenu.style.display = "block";
            });
            item.addEventListener("mouseleave", () => {
                subMenu.style.display = "none";
            });
        }
    });
});






// Variables
const carousel = document.querySelector('.carousel');
const prevButton = document.getElementById('prevButton');
const nextButton = document.getElementById('nextButton');
const images = document.querySelectorAll('.carousel img');
let currentIndex = 0;

// Función para mostrar la imagen actual
function showImage(index) {
    images.forEach((img, i) => {
        if (i === index) {
            img.style.display = 'block';
        } else {
            img.style.display = 'none';
        }
    });
}

// Mostrar la primera imagen
showImage(currentIndex);

// Evento para el botón siguiente
nextButton.addEventListener('click', () => {
    currentIndex++;
    if (currentIndex >= images.length) {
        currentIndex = 0;
    }
    showImage(currentIndex);
});

// Evento para el botón anterior
prevButton.addEventListener('click', () => {
    currentIndex--;
    if (currentIndex < 0) {
        currentIndex = images.length - 1;
    }
    showImage(currentIndex);
});